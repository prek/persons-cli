/*
 * Copyright 2013-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foo.people.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test showing the basic usage of {@link PersonRepository}.
 * Pre-populated values in resource /data.sql
 */
@RunWith(SpringRunner.class)
@Transactional
@DataJpaTest
@Sql("/data.sql")
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository repository;

    /**
     * Test repository getAll() method
     */
    @Test
    public void testGetAllMethod() {

        var  prepopulatedValues = repository.getAll().
                collect(Collectors.toList());
        List<Person> personList = repository.getAll().collect(Collectors.toList());
        prepopulatedValues.forEach(w ->
                assertThat(personList).contains(w));
    }

}
