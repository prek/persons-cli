package com.foo.people.cli;

import com.foo.people.persistence.Person;
import com.foo.people.service.PersonDao;
import com.foo.people.validators.IntegerPK;
import org.jline.utils.AttributedStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.ParameterValidationException;
import org.springframework.shell.Shell;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.shell.result.DefaultResultHandler;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.foo.people.service.PersonDaoMessageFormat.*;
import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT + ".enabled=false"
})
@Sql("/data.sql")
public class CliCommandsTest {

    private static final String INVALID_INTEGER_PK_MSG = "invalid integer PK value, must consist of 1-10 digits,between -2147483648 and 2147483647";

    @Autowired
    private Shell shell;

    @Autowired
    private PersonDao dao;

    @Autowired
    private DefaultResultHandler resultHandler;

    private String person1 = Person.of(1, "FirstName", "LastName").toString();
    private String person2 = Person.of(2, "AnotherFirst", "AnotherLast").toString();


    /**
     * Test that basic Spring Shell functionality is in place
     */
    @Test
    public void contextLoads() {
        Object help = shell.evaluate(() -> "help");
        assertThat(getCommandOutputContent(help)).contains("AVAILABLE COMMANDS");
    }

    /**
     * Test successful add operation
     */
    @Test
    public void testAddCommandSuccess() {
        Object output = shell.evaluate(() -> "add 3 NewFirst NewLast");
        assertThat((String) output).contains(Person.of(3, "NewFirst", "NewLast").toString());
    }

    /**
     * Test add  failure on duplicate id
     */
    @Test
    public void testAddFailOnExistingId() {
        Object output = shell.evaluate(() -> "add 1 NewFirst NewLast");
        assertThat((String) output).contains(String.format(ERROR_EXISTS_FMT, 1));
    }

    /**
     * Test add  failure on invalid id
     */
    @Test
    public void testAddFailOnInvalidId() {
        Object output = shell.evaluate(() -> "add Invalid NewFirst NewLast");
        assertThat( getValidationError(output)).contains(INVALID_INTEGER_PK_MSG);
    }

    /**
     * Test edit Command successful update
     */
    @Test
    public void testEditCommandSuccess() {
        Object output = shell.evaluate(() -> "edit 1 NewFirst NewLast");
        assertThat((String) output)
                .contains("Updated: " + Person.of(1, "NewFirst", "NewLast").toString());
    }

    /**
     * Test edit Command failure on non existing id
     */
    @Test
    public void testEditCommandFailOnNonExistingId() {
        Object output = shell.evaluate(() -> "edit 7 NewFirst NewLast");
        assertThat((String) output).contains(String.format(ERROR_NOT_EXISTS_FMT, 7));
    }

    /**
     * Test successful delete
     */
    @Test
    public void testDeleteCommandSuccess() {
        Object output = shell.evaluate(() -> "delete 1 ");
        assertThat((String) output).contains("Deleted : " + person1);
    }

    /**
     * Test failed delete due to non existing id
     */
    @Test
    public void testDeleteCommandFailOnNonExistingId() {
        Object output = shell.evaluate(() -> "delete 3 ");
        assertThat((String) output).contains(String.format(ERROR_NOT_EXISTS_FMT, 3));
    }

    /**
     * Test count operation
     */
    @Test
    public void testCountCommand() {
        Object output = shell.evaluate(() -> "count");
        assertThat((String) output).contains(String.format(MSG_TOTAL_ROWS, 2));
    }

    /**
     * Test list operation
     */
    @Test
    public void testListCommand() {
        Object output = shell.evaluate(() -> "list");
        assertThat((String) output).contains(String.format(MSG_TOTAL_ROWS, 2));
        assertThat((String) output).contains(person1);
        assertThat((String) output).contains(person2);
    }


    /*
     * Helper, getting the output of a shell command.
     */
    private String getCommandOutputContent(Object commandOutput) {
        return ((AttributedStringBuilder) commandOutput).toAnsi();
    }

    /*
     * Helper, getting the output of a shell command.
     */
    private String getValidationError(Object commandOutput) {
        return ((ParameterValidationException) commandOutput).getConstraintViolations().toArray()[0].toString();
    }



}