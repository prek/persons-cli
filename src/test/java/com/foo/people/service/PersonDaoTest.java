package com.foo.people.service;

import com.foo.people.persistence.Person;
import com.foo.people.persistence.PersonRepository;
import static  com.foo.people.service.PersonDaoMessageFormat.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration Test for PersonDao Component
 */
@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT + ".enabled=false"
})
@Sql("/data.sql")
public class PersonDaoTest {

    @Autowired
    private PersonRepository repository;
    @Autowired
    private PersonDao dao;

    private List<Person> prepopulatedValues;
    private String prepopulatedValuesListOutput;

    @Before
    public void setup() {
        prepopulatedValues = repository.getAll().collect(Collectors.toList());
        prepopulatedValuesListOutput = repository.getAll().map(Person::toString).collect(Collectors.joining("\n"));
    }

    /**
     * Test add method successful insert
     */
    @Test
    public void testAddMethodSuccess() {
        assertThat(dao.add(3,"NewFirst", "NewLast").contains(Person.of(3,"NewFirst", "NewLast").toString()));
    }

    /**
     * Test add method failure on duplicate id
     */
    @Test
    public void testAddMethodFailOnExistingId() {
        assertThat(dao.add(1,"NewFirst", "NewLast").contains(String.format(ERROR_EXISTS_FMT, 1)));
    }

    /**
     * Test edit method successful update
     */
    @Test
    public void testEditMethodSuccess() {
        assertThat(dao.edit(1,"NewFirst", "NewLast").contains("Updated: " +Person.of(1,"NewFirst", "NewLast").toString()));
    }

    /**
     * Test edit method failure on non existing id
     */
    @Test
    public void testEditMethodFailOnNonExistingId() {
        assertThat(dao.edit(7,"NewFirst", "NewLast").contains(String.format(ERROR_NOT_EXISTS_FMT, 7)));
    }

    /**
     * Test successful delete
     */
    @Test
    public void testDeleteMethodSuccess() {
        assertThat(dao.delete(1).contains("Deleted : " + prepopulatedValues.get(0).toString()));
    }

    /**
     * Test failed delete due to non existing id
     */
    @Test
    public void testDeleteMethodFailOnNonExistingId() {
        assertThat(dao.delete(3).contains(String.format(ERROR_NOT_EXISTS_FMT, 3)));
    }

    /**
     * Test count operation
     */
    @Test
    public void testCountMethod() {
        assertThat(dao.count().contains(String.format(MSG_TOTAL_ROWS, 2)));
    }

    /**
     * Test list operation
     */
    @Test
    public void testListMethod() {
        assertThat(dao.list().contains(prepopulatedValuesListOutput));
    }
}



