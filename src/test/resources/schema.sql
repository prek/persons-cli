create table persons (
    id int not null,
    first_name varchar(50) not null,
    last_name varchar(50) not null,
    primary key (id)
);