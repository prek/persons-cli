package com.foo.people.cli;

import com.foo.people.service.PersonDao;
import com.foo.people.validators.IntegerPK;
import com.foo.people.validators.PersonName;
import lombok.AllArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

/**
 * Implementation of shell commands
 */
@AllArgsConstructor
@ShellComponent
public class CliCommands {

    private PersonDao dao;

    @ShellMethod("Insert a new Person in the database.")
    public String add(@IntegerPK String id,
                      @PersonName String firstName,
                      @PersonName String lastName) {
        return dao.add(Integer.valueOf(id), firstName, lastName);
    }

    @ShellMethod("Update an existing Person in the database.")
    public String edit(@IntegerPK String id,
                       @PersonName String firstName,
                       @PersonName String lastName) {
        return dao.edit(Integer.valueOf(id), firstName, lastName);
    }

    @ShellMethod("Delete an existing Person from the database, given it's id.")
    public String delete(@IntegerPK String id) {
        return dao.delete(Integer.valueOf(id));
    }

    @ShellMethod("Count the number of Persons in the database.")
    public String count() {
        return dao.count();
    }

    @ShellMethod("List all Persons in the database")
    public String list() {
        return dao.list() + "\n" + dao.count();
    }

}
