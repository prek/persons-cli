package com.foo.people.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.lang.annotation.*;

/**
 * Custom validator to validate primary key values
 */
@Documented
@NotNull
@Digits(fraction = 0, integer = 10)
@Min(value = -2147483648)
@Max(value = 2147483647)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@ReportAsSingleViolation
public @interface IntegerPK {
    String message() default "invalid integer PK value, must consist of 1-10 digits,between -2147483648 and 2147483647";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
