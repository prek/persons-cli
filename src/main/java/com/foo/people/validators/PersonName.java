package com.foo.people.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.lang.annotation.*;

/**
 * Custom validator to validate first and last name values
 */
@Documented
@NotBlank
@Size(min = 1, max = 50)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@ReportAsSingleViolation
public @interface PersonName {
    String message() default "invalid name value, must be non empty and of length 1-50";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
