package com.foo.people.service;

import com.foo.people.persistence.Person;
import com.foo.people.persistence.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

import static com.foo.people.service.PersonDaoMessageFormat.*;

@Transactional
@Component
@AllArgsConstructor
public class PersonDaoImpl implements PersonDao {

    private final PersonRepository repository;

    @Override
    public String add(Integer id, String firstName, String lastName) {
        if (repository.existsById(id)) {
            return String.format(ERROR_EXISTS_FMT, id);
        }
        return "Added: " + repository.save(Person.of(id, firstName, lastName)).toString();
    }

    @Override
    public String edit(Integer id, String firstName, String lastName) {
        if (!repository.existsById(id)) {
            return String.format(ERROR_NOT_EXISTS_FMT, id);
        }
        return "Updated: " + repository.save(Person.of(id, firstName, lastName)).toString();
    }

    @Override
    public String delete(Integer id) {
        Optional<Person> existingPerson = repository.findById(id);
        if (existingPerson.isPresent()) {
            repository.deleteById(id);
            return "Deleted : " + existingPerson.get().toString();
        } else {
            return String.format(ERROR_NOT_EXISTS_FMT, id);
        }
    }

    @Override
    public String count() {
        return String.format(MSG_TOTAL_ROWS, repository.count());
    }

    @Override
    public String list() {
        return repository.getAll().map(Person::toString).collect(Collectors.joining("\n"));
    }
}
