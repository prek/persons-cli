package com.foo.people.service;

/**
 * String formats for PersonDao
 */
public class PersonDaoMessageFormat {

    private PersonDaoMessageFormat() {}

    public static final String ERROR_NOT_EXISTS_FMT = "ERROR - A Person with id: %d does not exists in the database";
    public static final String ERROR_EXISTS_FMT = "ERROR - A Person with id: %d already exists in the database";
    public static final String MSG_TOTAL_ROWS = "Total rows: %d";

}
