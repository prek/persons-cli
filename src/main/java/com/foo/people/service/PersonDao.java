package com.foo.people.service;

import com.foo.people.persistence.PersonRepository;

/**
 * Compound operations on top of basic repository operation as defined in {@link PersonRepository}
 */
public interface PersonDao {

    /**
     * Adds a new row count and returns the operation outcome in a readable format
     *
     * @param id,        columd 'id' value
     * @param firstName, column 'first_name' value
     * @param lastName,  column 'last_name' value
     */
    String add(Integer id, String firstName, String lastName);

    /**
     * Updates an existing Entity given its Id and the new values for firstName and lastName.
     *
     * @param id,        columd 'id' value
     * @param firstName, column 'first_name' value
     * @param lastName,  column 'last_name' value
     */
    String edit(Integer id, String firstName, String lastName);

    /**
     * Deletes an existing Entity given its Id
     *
     * @param id, columd 'id' value
     */
    String delete(Integer id);

    /**
     * Returns row count info in a readable format
     */
    String count();

    /**
     * Returns all entities,as a multiline string with each row in a separate line
     */
    String list();
}
