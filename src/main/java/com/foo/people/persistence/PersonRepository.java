package com.foo.people.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

/**
 * Repository for entity {@link Person}
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {

    /**
     * Return all Entities as a Java stream
     */
    @Query("select e from Person e")
    Stream<Person> getAll();

}
