package com.foo.people;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeopleCliApplication {

    public static void main(String[] args) {
        SpringApplication.run(PeopleCliApplication.class, args);
    }

}
